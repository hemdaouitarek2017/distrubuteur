import React from "react";
// react component that copies the given text inside your clipboard
import { CopyToClipboard } from "react-copy-to-clipboard";
// reactstrap components
import {
  Badge,
  Card,
  CardHeader,
  Pagination,
  PaginationItem,
  PaginationLink,
  Button,
  Table,
  Container,
  Row,
} from "reactstrap";
import Header from "components/Headers/Header.js";
import tools from "../../services/apis.js"
import block from "../../assets/img/block.png"
import ReactExport from "react-data-export";
import expo from "../../assets/img/export.png"
const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
class Icons extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      ListRevendeur: [],
      currentPage: 0,
    }
  }
  handleClick(e, index) {

    e.preventDefault();

    this.setState({
      currentPage: index
    });

  }

  componentWillMount() {
    this.getRevenndeur()
    this.pageSize = 10;
  }
  async getRevenndeur() {
    await fetch(tools.getRvendeurByDist(1), {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(results => {
        return results.json()
      }).then(data => {
        console.log(data)
        this.pagesCount = Math.ceil(data.length / this.pageSize);
        this.setState({ ListRevendeur: data })
      })
  }
  async bloqueRevendeur(id, nom, prenom) {
    await fetch(tools.bloqueRevendeur(id), {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(results => {
        return results
      }).then(data => {
        alert("Le revendeur " + nom + " " + prenom + " a éte bloque")
        console.log(data)
        this.setState({
          ListRevendeur: this.state.ListRevendeur.filter(item => item.id !== id),
        });
      })
  }
  showRes(item) {
    if (item.blocked != 1) {
      return (
        <tr>
          <td>{item.nom}</td>
          <td>{item.prenom}</td>
          <td>{item.telephone}</td>
          <td>{item.adresse}</td>
          <td>{item.mf}</td>
          <td>{item.rc}</td>
          <td>{item.comm}</td>
          <td>
            <Button onClick={() => this.bloqueRevendeur(item.id, item.nom, item.prenom)} className="btn-icon btn-2" color="danger" type="button">
              <span className="btn-inner--icon">
                <img src={block} />
              </span>
            </Button>
          </td>
        </tr>
      )
    }
  }
  render() {
    let data = []
    this.state.ListRevendeur.map(t => {
      let field = []
      field = [{ value: t.nom }, { value: t.pronom }, { value: t.mf }, { value: t.rc }, { value: t.telephone }, { value: t.adresse }]
      data.push(field)
    }
    )
    console.log(data)
    const multiDataSet = [
      {
        columns: [
          { title: "Nom", width: { wch: 20 } },//pixels width 
          { title: "Prenom", width: { wch: 20 } },//char width 
          { title: "MF", width: { wch: 20 } },
          { title: "RC", width: { wch: 20 } },
          { title: "Telephone", width: { wch: 20 } },
          { title: "Adresse", width: { wch: 20 } },
        ],
        data: data
      }
    ];
    const { currentPage } = this.state;
    return (
      <>
        <Header />
        {/* Page content */}
        <Container className="mt--7" fluid>
          {/* Table */}
          <Row>
            <div className="col">
              <Card className="shadow">
                <CardHeader className="border-0">
                  <h3 className="mb-0">Liste Des Revendeurs</h3>
                </CardHeader>
                <ExcelFile filename="List Revendeurs" element={<Button style={{ marginLeft: 20, marginBottom: 20 }} className="btn-icon btn-2" color="success" type="button">
                  <span className="btn-inner--icon">
                    <img src={expo} />
                    <span className="btn-inner--text">Export fichier excel</span>
                  </span>
                </Button>}>
                  <ExcelSheet dataSet={multiDataSet} name="Organization" />
                </ExcelFile>
                <Table className="align-items-center table-flush" responsive>
                  <thead className="thead-light">
                    <tr>
                      <th scope="col">Nom</th>
                      <th scope="col">Prenom</th>
                      <th scope="col">Télephone</th>
                      <th scope="col" >Adresse</th>
                      <th scope="col">MF</th>
                      <th scope="col">RC</th>
                      <th scope="col">Commission</th>
                      <th scope="col">Bloquer</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      this.state.ListRevendeur.slice(
                        currentPage * this.pageSize,
                        (currentPage + 1) * this.pageSize
                      ).map((t) => this.showRes(t))
                    }
                  </tbody>
                </Table>
                <div className="pagination-wrapper">
                  <Pagination style={{ padding: 10 }} className="pagination justify-content-end mb-0"
                    listClassName="justify-content-end mb-0">
                    <PaginationItem disabled={currentPage <= 0}>
                      <PaginationLink
                        onClick={e => this.handleClick(e, currentPage - 1)}
                        previous
                        href="#"
                      />
                    </PaginationItem>
                    {[...Array(this.pagesCount)].map((page, i) =>
                      <PaginationItem active={i === currentPage} key={i}>
                        <PaginationLink onClick={e => this.handleClick(e, i)} href="#">
                          {i + 1}
                        </PaginationLink>
                      </PaginationItem>
                    )}
                    <PaginationItem disabled={currentPage >= this.pagesCount - 1}>
                      <PaginationLink
                        onClick={e => this.handleClick(e, currentPage + 1)}
                        next
                        href="#"
                      />
                    </PaginationItem>
                  </Pagination>
                </div>
              </Card>
            </div>
          </Row>
        </Container>
      </>
    );
  }
}

export default Icons;
